#define _GNU_SOURCE

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <pthread.h>
#include <sched.h>
#include <x86intrin.h>

typedef enum {
  P_DEFAULT = 0,
  P_SPREAD  = 1,
  P_PINNED  = 2
} placement_policy_t;

typedef struct {
  int cores;
  placement_policy_t policy;
  pthread_barrier_t time_barrier;
  pthread_barrier_t barrier;
  volatile long counter;
  pthread_mutex_t mutex;
} global_t;

global_t global;

typedef struct {
  int n;
  int i;
  int iter;
} local_t;

void apply_placement_policy(local_t local) {
  if (global.policy != P_DEFAULT) {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(local.i % global.cores, &cpuset);

    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
  }

  int cpu1 = sched_getcpu();
  printf("[1] Thread %3d initialized on cpu %3d\n", local.i, cpu1);

  if (global.policy != P_PINNED) {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    for (int i = 0; i < global.cores; i++) {
      CPU_SET(i, &cpuset);
    }
    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
  }

  int cpu2 = sched_getcpu();
  printf("[2] Thread %3d initialized on cpu %3d\n", local.i, cpu2);
}

void* thread_start(void* local_ptr) {
  local_t local = *(local_t*) local_ptr;
  free(local_ptr);

  apply_placement_policy(local);

  pthread_barrier_wait(&global.time_barrier);

  volatile int sum = 0;
  for (int i = 0; i < local.iter; i++) {
    global.counter = local.n;

    pthread_barrier_wait(&global.barrier);

    for (volatile int j = 0; j < 10; j++) {
      sum += 1;
    }

    pthread_mutex_lock(&global.mutex);
    global.counter--;
    pthread_mutex_unlock(&global.mutex);
    while (global.counter);

    for (volatile int j = 0; j < 10; j++) {
      sum += 1;
    }

    pthread_barrier_wait(&global.barrier);
  }

  pthread_barrier_wait(&global.time_barrier);

  int* holder = malloc(sizeof(int));
  *holder = sum;
  pthread_exit(holder);
}

int main(int argc, char** argv) {
  if (argc != 5) {
    printf("Usage: %s <cores> <placement policy> <threads> <iterations>\n", argv[0]);
    return 1;
  }

  global.cores = atoi(argv[1]);
  if (strncmp(argv[2], "default", strlen(argv[2])) == 0) {
    global.policy = P_DEFAULT;
  }
  else if (strncmp(argv[2], "spread", strlen(argv[2])) == 0) {
    global.policy = P_SPREAD;
  }
  else if (strncmp(argv[2], "pinned", strlen(argv[2])) == 0) {
    global.policy = P_PINNED;
  }
  else {
    printf("Wrong placement policy: %s\n", argv[2]);
    return 2;
  }

  int n = atoi(argv[3]);
  int iter = atoi(argv[4]);
  pthread_t threads[n];

  pthread_barrier_init(&global.time_barrier, NULL, n + 1);
  pthread_barrier_init(&global.barrier, NULL, n);
  pthread_mutex_init(&global.mutex, NULL);

  srand48(0);
  for (int i = 0; i < n; i++) {
    local_t* local = malloc(sizeof(local_t));
    local->n = n;
    local->i = i;
    local->iter = iter;

    pthread_create(&threads[i], NULL, thread_start, local);
  }

  uint64_t _start = _rdtsc();
  uint64_t _end = _rdtsc();
  uint64_t residu = _end - _start;

  pthread_barrier_wait(&global.time_barrier);

  uint64_t start = _rdtsc();
  pthread_barrier_wait(&global.time_barrier);

  uint64_t end = _rdtsc();

  int expected = iter * 20;
  for (int i = 0; i < n; i++) {
    void* holder;
    pthread_join(threads[i], &holder);
    int result = * (int*) holder;
    free(holder);

    assert(expected == result);
  }

  fprintf(stderr, "%ld\n", end - start - residu);

  pthread_barrier_destroy(&global.barrier);
  pthread_mutex_destroy(&global.mutex);

  return 0;
}

