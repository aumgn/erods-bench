export BARNES_DIR=$PARSEC_DIR/ext/splash2x/apps/barnes
export BARNES_BIN=$BARNES_DIR/inst/amd64-linux.gcc-pthreads/bin/barnes
export BARNES_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/ext/splash2x/apps/barnes/inputs/input_native.tar
export BARNES_INPUTS_DIR=$TMP_INPUTS_DIR/barnes

prepare_barnes() {
    if [[ ! -f $BARNES_INPUTS_DIR/head.opc ]]; then
        mkdir -p $BARNES_INPUTS_DIR
        tar -xf $BARNES_INPUTS_ARCHIVE -C $BARNES_INPUTS_DIR
    fi
    
    for nodes in $nodes_list; do
        threads=$(( $nodes * 8 ))
        sed "s/NUMPROCS/$threads/" $BARNES_INPUTS_DIR/input.template > $BARNES_INPUTS_DIR/input.$threads
    done
}

dump_barnes() {
    sha1sum $BARNES_BIN          >>$dump_program_dir/barnes
    sha1sum $BARNES_INPUTS_DIR/* >>$dump_program_dir/barnes
}

command_barnes() {
    echo '$cmd_prefix $BARNES_BIN $threads < $BARNES_INPUTS_DIR/input.$threads'
}

