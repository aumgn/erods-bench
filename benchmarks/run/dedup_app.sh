export DEDUP_APP_DIR=$PARSEC_DIR/pkgs/kernels/dedup
export DEDUP_APP_BIN=$DEDUP_APP_DIR/inst/amd64-linux.gcc-pthreads/bin/dedup
export DEDUP_APP_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/pkgs/kernels/dedup/inputs/input_native.tar
export DEDUP_APP_INPUTS_DIR=$TMP_INPUTS_DIR/dedup

prepare_dedup_app() {
    if [[ ! -f $DEDUP_APP_INPUTS_DIR/FC-6-x86_64-disc1.iso ]]; then
        mkdir -p $DEDUP_APP_INPUTS_DIR
        tar -xf $DEDUP_APP_INPUTS_ARCHIVE -C $DEDUP_APP_INPUTS_DIR
    fi
}

dump_dedup_app() {
    local inputs=$(find $DEDUP_APP_INPUTS_DIR -type f -name '*.iso')

    sha1sum $DEDUP_APP_BIN $inputs >>$dump_program_dir/dedup_app
}

command_dedup_app() {
    dedup_threads=$(( ($threads-2) / 3 ))
    echo $cmd_prefix $DEDUP_APP_BIN -c -p -v -t $dedup_threads \
        -i $DEDUP_APP_INPUTS_DIR/FC-6-x86_64-disc1.iso \
        -o $DEDUP_APP_INPUTS_DIR/output.dat.ddp
}

