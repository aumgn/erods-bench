WORD_COUNT_MR_DIR=$PHOENIX_DIR/phoenix-2.0/tests/word_count
WORD_COUNT_MR_BIN=$WORD_COUNT_MR_DIR/word_count
WORD_COUNT_MR_INPUTS_ARCHIVE=$BENCH_PATH/tmp/word_count_input.tar.gz
WORD_COUNT_MR_INPUTS_DIR=$TMP_INPUTS_DIR/word_count

prepare_word_count_mr() {
  if [[ ! -f $WORD_COUNT_MR_INPUTS_DIR/word_100MB.txt ]]; then
      mkdir -p $WORD_COUNT_MR_INPUTS_DIR
      tar -xzf $WORD_COUNT_MR_INPUTS_ARCHIVE -C $WORD_COUNT_MR_INPUTS_DIR --strip-components 1
  fi

  if [ ! -f $WORD_COUNT_MR_INPUTS_DIR/input ]; then
      for i in $(seq 1 15); do
          cat $WORD_COUNT_MR_INPUTS_DIR/word_100MB.txt >> $WORD_COUNT_MR_INPUTS_DIR/input
      done
  fi
}

dump_word_count_mr() {
    sha1sum $WORD_COUNT_MR_BIN              >>$dump_program_dir/word_count_mr
    sha1sum $WORD_COUNT_MR_INPUTS_DIR/input >>$dump_program_dir/word_count_mr
}

command_word_count_mr() {
    echo MR_NUMPROCS=$threads MAPRED_NO_BINDING=1 $cmd_prefix $WORD_COUNT_MR_BIN $WORD_COUNT_MR_INPUTS_DIR/input
}

