export VIPS_DIR=$PARSEC_DIR/pkgs/apps/vips
export VIPS_BIN=$VIPS_DIR/inst/amd64-linux.gcc-pthreads/bin/vips
export VIPS_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/pkgs/apps/vips/inputs/input_native.tar
export VIPS_INPUTS_DIR=$TMP_INPUTS_DIR/vips
export VIPS_ARG_SIZE=500

prepare_vips() {
    if [[ ! -f $VIPS_INPUTS_DIR/vips ]]; then
        mkdir -p $VIPS_INPUTS_DIR
        tar -xf $VIPS_INPUTS_ARCHIVE -C $VIPS_INPUTS_DIR
    fi
}

dump_vips() {
    sha1sum $VIPS_BIN          >>$dump_program_dir/vips
    sha1sum $VIPS_INPUTS_DIR/* >>$dump_program_dir/vips
}

command_vips() {
    echo  IM_CONCURRENCY=$threads $cmd_prefix $VIPS_BIN im_benchmark \
        $VIPS_INPUTS_DIR/orion_18000x18000.v $VIPS_INPUTS_DIR/output.v
}

