HISTOGRAM_MR_DIR=$PHOENIX_DIR/phoenix-2.0/tests/histogram
HISTOGRAM_MR_BIN=$HISTOGRAM_MR_DIR/histogram
HISTOGRAM_MR_INPUT=$BENCH_PATH/tmp/histogram_input.bmp
HISTOGRAM_MR_INPUTS_DIR=$TMP_INPUTS_DIR/histogram

prepare_histogram_mr() {
    if [ ! -f $HISTOGRAM_MR_INPUTS_DIR/input ]; then
	mkdir -p $HISTOGRAM_MR_INPUTS_DIR
	mv $HISTOGRAM_MR_INPUT $HISTOGRAM_MR_INPUTS_DIR/input
    fi
}

dump_histogram_mr() {
    sha1sum $HISTOGRAM_MR_BIN              >>$dump_program_dir/histogram_mr
    sha1sum $HISTOGRAM_MR_INPUTS_DIR/input >>$dump_program_dir/histogram_mr
}

command_histogram_mr() {
    echo MR_NUMPROCS=$threads MAPRED_NO_BINDING=1 $cmd_prefix $HISTOGRAM_MR_BIN $HISTOGRAM_MR_INPUTS_DIR/input
}

