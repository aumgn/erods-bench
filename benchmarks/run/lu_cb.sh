export LU_CB_DIR=$PARSEC_DIR/ext/splash2x/kernels/lu_cb
export LU_CB_BIN=$LU_CB_DIR/inst/amd64-linux.gcc-pthreads/bin/lu_cb

prepare_lu_cb() {
    true
}

dump_lu_cb() {
    sha1sum $LU_CB_BIN >>$dump_program_dir/lu_cb
}

command_lu_cb() {
    echo $cmd_prefix $LU_CB_BIN -p$threads -n8096 -b32
}

