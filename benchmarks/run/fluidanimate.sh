export FLUIDANIMATE_DIR=$PARSEC_DIR/pkgs/apps/fluidanimate
export FLUIDANIMATE_BIN=$FLUIDANIMATE_DIR/inst/amd64-linux.gcc-pthreads/bin/fluidanimate
export FLUIDANIMATE_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/pkgs/apps/fluidanimate/inputs/input_native.tar
export FLUIDANIMATE_INPUTS_DIR=$TMP_INPUTS_DIR/fluidanimate
export FLUIDANIMATE_INPUT=$FLUIDANIMATE_INPUTS_DIR/in_500K.fluid

prepare_fluidanimate() {
    if [[ ! -f $FLUIDANIMATE_INPUT ]]; then
        mkdir -p $FLUIDANIMATE_INPUTS_DIR
        tar -xf $FLUIDANIMATE_INPUTS_ARCHIVE -C $FLUIDANIMATE_INPUTS_DIR
    fi
}

dump_fluidanimate() {
    sha1sum $FLUIDANIMATE_BIN $FLUIDANIMATE_INPUT >>$dump_program_dir/fluidanimate
}

command_fluidanimate() {
    echo $cmd_prefix $FLUIDANIMATE_BIN $threads 500 $FLUIDANIMATE_INPUT $FLUIDANIMATE_INPUTS_DIR/out.fluid
}

