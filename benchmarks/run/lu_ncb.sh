export LU_NCB_DIR=$PARSEC_DIR/ext/splash2x/kernels/lu_ncb
export LU_NCB_BIN=$LU_NCB_DIR/inst/amd64-linux.gcc-pthreads/bin/lu_ncb

prepare_lu_ncb() {
    true
}

dump_lu_ncb() {
    sha1sum $LU_NCB_BIN >>$dump_program_dir/lu_ncb
}

command_lu_ncb() {
    echo $cmd_prefix $LU_NCB_BIN -p$threads -n8096 -b32
}

