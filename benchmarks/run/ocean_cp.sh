export OCEAN_CP_DIR=$PARSEC_DIR/ext/splash2x/apps/ocean_cp
export OCEAN_CP_BIN=$OCEAN_CP_DIR/inst/amd64-linux.gcc-pthreads/bin/ocean_cp

prepare_ocean_cp() {
    true
}

dump_ocean_cp() {
    sha1sum $OCEAN_CP_BIN >>$dump_program_dir/ocean_cp
}

command_ocean_cp() {
    echo $cmd_prefix $OCEAN_CP_BIN -n2050 -p$threads -e1e-07 -r10000 -t14400

}

