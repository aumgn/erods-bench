export BODYTRACK_DIR=$PARSEC_DIR/pkgs/apps/bodytrack
export BODYTRACK_BIN=$BODYTRACK_DIR/inst/amd64-linux.gcc-pthreads/bin/bodytrack
export BODYTRACK_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/pkgs/apps/bodytrack/inputs/input_native.tar
export BODYTRACK_INPUTS_DIR=$TMP_INPUTS_DIR/bodytrack

prepare_bodytrack() {
    if [[ ! -f $BODYTRACK_INPUTS_DIR/BodyShapeParameters.txt ]]; then
        mkdir -p $BODYTRACK_INPUTS_DIR
        tar -xf $BODYTRACK_INPUTS_ARCHIVE -C $BODYTRACK_INPUTS_DIR
    fi
}

dump_bodytrack() {
    local inputs=$(find tmpfs/inputs/bodytrack/sequenceB_261/* -type f -name '*.bmp')

    sha1sum $BODYTRACK_BIN $inputs >>$dump_program_dir/bodytrack
}

command_bodytrack() {
    echo $cmd_prefix $BODYTRACK_BIN $BODYTRACK_INPUTS_DIR/sequenceB_261 4 261 4000 5 0 $threads
}

