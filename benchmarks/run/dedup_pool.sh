export DEDUP_POOL_DIR=$PARSEC_DIR/pkgs/kernels/dedup
export DEDUP_POOL_BIN=$DEDUP_POOL_DIR/inst/amd64-linux.gcc-pthreads/bin/dedup
export DEDUP_POOL_INPUTS_ARCHIVE=$BENCH_PATH/tmp/parsec-inputs/pkgs/kernels/dedup/inputs/input_native.tar
export DEDUP_POOL_INPUTS_DIR=$TMP_INPUTS_DIR/dedup

prepare_dedup_pool() {
    if [[ ! -f $DEDUP_POOL_INPUTS_DIR/FC-6-x86_64-disc1.iso ]]; then
        mkdir -p $DEDUP_POOL_INPUTS_DIR
        tar -xf $DEDUP_POOL_INPUTS_ARCHIVE -C $DEDUP_POOL_INPUTS_DIR
    fi
}

dump_dedup_pool() {
    local inputs=$(find $DEDUP_POOL_INPUTS_DIR -type f -name '*.iso')

    sha1sum $DEDUP_POOL_BIN $inputs >>$dump_program_dir/dedup_pool
}

command_dedup_pool() {
    echo $cmd_prefix $DEDUP_POOL_BIN -c -p -v -t $threads \
        -i $DEDUP_POOL_INPUTS_DIR/FC-6-x86_64-disc1.iso \
        -o $DEDUP_POOL_INPUTS_DIR/output.dat.ddp
}

