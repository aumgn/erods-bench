export STREAMCLUSTER_DIR=$PARSEC_DIR/pkgs/kernels/streamcluster
export STREAMCLUSTER_BIN=$STREAMCLUSTER_DIR/inst/amd64-linux.gcc-pthreads/bin/streamcluster

prepare_streamcluster() {
    true
}

dump_streamcluster() {
    sha1sum $STREAMCLUSTER_BIN >>$dump_program_dir/streamcluster
}

command_streamcluster() {
    echo $cmd_prefix $STREAMCLUSTER_BIN 10 20 128 16384 16384 1000 none /dev/null $threads
}

