PARSECMGMT=benchmarks/parsec/bin/parsecmgmt
PARSEC_INPUTS_URL=http://parsec.cs.princeton.edu/download/3.0/parsec-3.0-input-native.tar.gz
PARSEC_INPUTS_ARCHIVE=tmp/parsec-inputs.tar.gz
PARSEC_INPUTS=tmp/parsec-inputs
PARSEC_PROGRAMS=\
	parsec.bodytrack \
	parsec.streamcluster \
	splash2x.volrend \
	splash2x.lu_cb \
	splash2x.lu_ncb \
	splash2x.barnes \
	parsec.fluidanimate \
	parsec.vips \
	parsec.dedup \
	splash2x.ocean_cp

WORD_COUNT_INPUTS_URL=http://csl.stanford.edu/~christos/data/word_count.tar.gz
WORD_COUNT_INPUTS_ARCHIVE=tmp/word_count_input.tar.gz
HISTOGRAM_INPUT=tmp/histogram_input.bmp
LIN_REG_INPUTS_URL=http://csl.stanford.edu/~christos/data/linear_regression.tar.gz
LIN_REG_INPUTS_ARCHIVE=tmp/lin_reg_input.tar.gz

MOSBENCH_INPUTS_URL=https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.9.9.tar.gz
MOSBENCH_INPUTS_ARCHIVE=tmp/linux-4.9.9.tar.gz
MOSBENCH_INPUTS=tmp/linux-4.9.9

.PHONY: install
install: submodules \
         $(PARSEC_INPUTS) \
         $(WORD_COUNT_INPUTS_ARCHIVE) \
         $(HISTOGRAM_INPUT) \
         $(LIN_REG_INPUTS_ARCHIVE) \
         $(MOSBENCH_INPUTS) \

.PHONY: submodules
submodules:
	git submodule update --init
	make -Ctools/PinThreads
	make -Cbenchmarks/phoenix/phoenix-2.0
	make -C. build-parsec
	make -Cbenchmarks/mini-volrend

BUILD_PARSEC_TARGETS=$(patsubst %,build-%,$(PARSEC_PROGRAMS))
.PHONY: build-parsec
build-parsec: $(BUILD_PARSEC_TARGETS)

.PHONY: $(BUILD_PARSEC_TARGETS)
$(BUILD_PARSEC_TARGETS): build-%:
	$(PARSECMGMT) -a build -p $* -c gcc-pthreads

$(PARSEC_INPUTS_ARCHIVE):
	mkdir -p tmp/
	@echo "### Downloading inputs for Parsec (can take a while)"
	wget $(PARSEC_INPUTS_URL) -O $(PARSEC_INPUTS_ARCHIVE)

$(PARSEC_INPUTS): $(PARSEC_INPUTS_ARCHIVE)
	mkdir -p $(PARSEC_INPUTS)
	@echo "### Uncompressing inputs for Parsec (can take a while)"
	tar xzf $(PARSEC_INPUTS_ARCHIVE) -C$(PARSEC_INPUTS) --strip-components 1

$(WORD_COUNT_INPUTS_ARCHIVE):
	mkdir -p tmp/
	@echo "### Downloading input for word_count (can take a while)"
	wget $(WORD_COUNT_INPUTS_URL) -O $(WORD_COUNT_INPUTS_ARCHIVE)

$(HISTOGRAM_INPUT):
	mkdir -p tmp/
	@echo "# Generating input for histogram (can take a while)"
	convert -size 30000x30000 xc:gray +noise random $(HISTOGRAM_INPUT)

$(LIN_REG_INPUTS_ARCHIVE):
	mkdir -p tmp/
	@echo "### Downloading input for lin_reg (can take a while)"
	wget $(LIN_REG_INPUTS_URL) -O $(LIN_REG_INPUTS_ARCHIVE)

$(MOSBENCH_INPUTS_ARCHIVE):
	mkdir -p tmp/
	@echo "### Downloading input for mosbench (can take a while)"
	wget $(MOSBENCH_INPUTS_URL) -O $(MOSBENCH_INPUTS_ARCHIVE)

$(MOSBENCH_INPUTS): $(MOSBENCH_INPUTS_ARCHIVE)
	tar -xzf $(MOSBENCH_INPUTS_ARCHIVE) -C tmp

CLEAN_PARSEC_TARGETS=$(patsubst %,clean-%,$(PARSEC_PROGRAMS))
.PHONY: clean-parsec
clean-parsec: $(CLEAN_PARSEC_TARGETS)

.PHONY: $(CLEAN_PARSEC_TARGETS)
$(CLEAN_PARSEC_TARGETS): clean-%:
	$(PARSECMGMT) -a uninstall -p $* -c gcc-pthreads

.PHONY: clean
clean:
	rm -rf tmp
	make -Ctools/PinThreads clean
	make -Csrc/mini-volrend clean
	make -Cbenchmarks/phoenix/phoenix-2.0 clean
	git submodule deinit --force .

