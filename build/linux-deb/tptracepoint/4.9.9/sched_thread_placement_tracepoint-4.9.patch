diff --git a/include/linux/sched.h b/include/linux/sched.h
index 75d9a57..17a6010 100644
--- a/include/linux/sched.h
+++ b/include/linux/sched.h
@@ -1472,6 +1472,31 @@ struct tlbflush_unmap_batch {
 	bool writable;
 };
 
+#define TP_TRIGGER_NONE             0x0000
+#define TP_TRIGGER_STD_PERIODIC_LB  0x0001
+#define TP_TRIGGER_NOHZ_PERIODIC_LB 0x0002
+#define TP_TRIGGER_IDLE_LB          0x0004
+#define TP_TRIGGER_WAKE_UP_LB       0x0008
+#define TP_TRIGGER_WAKE_AFFINE      0x0010
+#define TP_TRIGGER_NEW_THREAD       0x0020
+#define TP_TRIGGER_EXEC             0x0040
+//#define TP_TRIGGER_RT             0x0080
+#define TP_TRIGGER_REALTIME         0x0080
+//#define TP_TRIGGER_DL             0x0100
+#define TP_TRIGGER_DEADLINE         0x0100
+//#define TP_TRIGGER_NB             0x0200
+#define TP_TRIGGER_NUMA_BALANCING   0x0200
+#define TP_TRIGGER_CPUS_ALLOWED     0x0400
+//#define TP_TRIGGER_HP             0x0800
+#define TP_TRIGGER_HOTPLUG          0x0800
+#define TP_TRIGGER_OTHER            0x1000
+typedef int tp_trigger_t;
+
+#define LU_TRIGGER_PERIODIC 0x01
+#define LU_TRIGGER_ENQUEUE  0x02
+#define LU_TRIGGER_DEQUEUE  0x03
+typedef int lu_trigger_t;
+
 struct task_struct {
 #ifdef CONFIG_THREAD_INFO_IN_TASK
 	/*
@@ -1497,6 +1522,7 @@ struct task_struct {
 	struct task_struct *last_wakee;
 
 	int wake_cpu;
+	tp_trigger_t trigger;
 #endif
 	int on_rq;
 
diff --git a/include/trace/events/sched.h b/include/trace/events/sched.h
index 9b90c57..12d937f 100644
--- a/include/trace/events/sched.h
+++ b/include/trace/events/sched.h
@@ -8,6 +8,25 @@
 #include <linux/tracepoint.h>
 #include <linux/binfmts.h>
 
+#define __print_tp_triggers(triggers) ((triggers) ? __print_flags(triggers, "|", \
+		{ TP_TRIGGER_STD_PERIODIC_LB, "SLB" },  \
+		{ TP_TRIGGER_NOHZ_PERIODIC_LB, "NLB" }, \
+		{ TP_TRIGGER_IDLE_LB, "ILB" },         	\
+		{ TP_TRIGGER_WAKE_UP_LB, "WLB" },       \
+		{ TP_TRIGGER_WAKE_AFFINE, "WA" },       \
+		{ TP_TRIGGER_NEW_THREAD, "FLB" },       \
+		{ TP_TRIGGER_EXEC, "ELB" },             \
+		{ TP_TRIGGER_REALTIME, "RT" },          \
+		{ TP_TRIGGER_DEADLINE, "DL" },          \
+		{ TP_TRIGGER_NUMA_BALANCING, "NB" },    \
+		{ TP_TRIGGER_CPUS_ALLOWED, "CA" },      \
+		{ TP_TRIGGER_HOTPLUG, "HP" },           \
+		{ TP_TRIGGER_OTHER, "?" }) : "X")
+
+#define __print_load_update_trigger(trigger) (__print_symbolic(trigger, \
+		{ LU_TRIGGER_PERIODIC, "PER" },  \
+		{ LU_TRIGGER_ENQUEUE, "ENQ" }, \
+		{ LU_TRIGGER_DEQUEUE, "DEQ" }))
 /*
  * Tracepoint for calling kthread_stop, performed to end a kthread:
  */
@@ -191,6 +210,66 @@ TRACE_EVENT(sched_migrate_task,
 		  __entry->orig_cpu, __entry->dest_cpu)
 );
 
+TRACE_EVENT(sched_thread_placement,
+
+	TP_PROTO(struct task_struct *p, int dest_cpu, tp_trigger_t trigger, int level),
+
+	TP_ARGS(p, dest_cpu, trigger, level),
+
+	TP_STRUCT__entry(
+		__array(          char,	comm,	TASK_COMM_LEN	)
+		__field(         pid_t,	pid			)
+		__field(           int,	orig_cpu		)
+		__field(           int,	dest_cpu		)
+		__field(           int,	exec_cpu		)
+		__field(           int,	trigger			)
+		__field(           int,	level			)
+		__field( unsigned long,	task_load		)
+	),
+
+	TP_fast_assign(
+		memcpy(__entry->comm, p->comm, TASK_COMM_LEN);
+		__entry->pid		= p->pid;
+		__entry->orig_cpu	= task_cpu(p);
+		__entry->dest_cpu	= dest_cpu;
+		__entry->exec_cpu       = smp_processor_id();
+		__entry->trigger        = trigger;
+		__entry->level		= level;
+		__entry->task_load	= p->se.avg.load_avg;
+	),
+
+	TP_printk("comm=%s pid=%d orig_cpu=%d dest_cpu=%d exec_cpu=%d trigger=%s level=%d task_load=%lu",
+		__entry->comm, __entry->pid,
+		__entry->orig_cpu, __entry->dest_cpu, __entry->exec_cpu,
+		__print_tp_triggers(__entry->trigger), __entry->level, __entry->task_load)
+);
+
+TRACE_EVENT(sched_load_update,
+
+	TP_PROTO(lu_trigger_t trigger, int cpu, int idx, unsigned long value),
+
+	TP_ARGS(trigger, cpu, idx, value),
+
+	TP_STRUCT__entry(
+		__field(	   int,     	trigger )
+		__field(	   int,	          cpu )
+		__field(	   int,	          idx	)
+		__field( unsigned long,	  value )
+	),
+
+	TP_fast_assign(
+		__entry->trigger = trigger;
+		__entry->cpu	= cpu;
+		__entry->idx	= idx;
+		__entry->value	= value;
+	),
+
+	TP_printk("trigger=%s cpu=%d idx=%d value=%lu",
+			__print_load_update_trigger(__entry->trigger),
+			__entry->cpu, __entry->idx, __entry->value)
+);
+
+
 DECLARE_EVENT_CLASS(sched_process_template,
 
 	TP_PROTO(struct task_struct *p),
diff --git a/kernel/sched/core.c b/kernel/sched/core.c
index 154fd68..d3dc15e 100644
--- a/kernel/sched/core.c
+++ b/kernel/sched/core.c
@@ -1015,6 +1015,7 @@ static struct rq *move_queued_task(struct rq *rq, struct task_struct *p, int new
 struct migration_arg {
 	struct task_struct *task;
 	int dest_cpu;
+	tp_trigger_t trigger;
 };
 
 /*
@@ -1026,7 +1027,7 @@ struct migration_arg {
  * So we race with normal scheduler movements, but that's OK, as long
  * as the task is no longer on this CPU.
  */
-static struct rq *__migrate_task(struct rq *rq, struct task_struct *p, int dest_cpu)
+static struct rq *__migrate_task(struct rq *rq, struct task_struct *p, int dest_cpu, tp_trigger_t trigger)
 {
 	if (unlikely(!cpu_active(dest_cpu)))
 		return rq;
@@ -1035,6 +1036,7 @@ static struct rq *__migrate_task(struct rq *rq, struct task_struct *p, int dest_
 	if (!cpumask_test_cpu(dest_cpu, tsk_cpus_allowed(p)))
 		return rq;
 
+	p->trigger = trigger;
 	rq = move_queued_task(rq, p, dest_cpu);
 
 	return rq;
@@ -1072,7 +1074,7 @@ static int migration_cpu_stop(void *data)
 	 */
 	if (task_rq(p) == rq) {
 		if (task_on_rq_queued(p))
-			rq = __migrate_task(rq, p, arg->dest_cpu);
+			rq = __migrate_task(rq, p, arg->dest_cpu, arg->trigger);
 		else
 			p->wake_cpu = arg->dest_cpu;
 	}
@@ -1184,7 +1186,7 @@ static int __set_cpus_allowed_ptr(struct task_struct *p,
 
 	dest_cpu = cpumask_any_and(cpu_valid_mask, new_mask);
 	if (task_running(rq, p) || p->state == TASK_WAKING) {
-		struct migration_arg arg = { p, dest_cpu };
+		struct migration_arg arg = { p, dest_cpu, TP_TRIGGER_CPUS_ALLOWED };
 		/* Need help from migration thread: drop lock and wait. */
 		task_rq_unlock(rq, p, &rf);
 		stop_one_cpu(cpu_of(rq), migration_cpu_stop, &arg);
@@ -1196,6 +1198,7 @@ static int __set_cpus_allowed_ptr(struct task_struct *p,
 		 * afterwards anyway.
 		 */
 		lockdep_unpin_lock(&rq->lock, rf.cookie);
+		p->trigger = TP_TRIGGER_CPUS_ALLOWED;
 		rq = move_queued_task(rq, p, dest_cpu);
 		lockdep_repin_lock(&rq->lock, rf.cookie);
 	}
@@ -1211,6 +1214,47 @@ int set_cpus_allowed_ptr(struct task_struct *p, const struct cpumask *new_mask)
 }
 EXPORT_SYMBOL_GPL(set_cpus_allowed_ptr);
 
+inline int lowest_sd_level_containing_both(int cpu1, int cpu2)
+{
+	int level = 0;
+	struct sched_domain *sd;
+
+	for_each_domain(cpu1, sd) {
+		if (cpumask_test_cpu(cpu2, sched_domain_span(sd)))
+			break;
+		level++;
+	}
+
+	return level;
+}
+
+inline void __set_task_cpu(struct task_struct *p, unsigned int cpu)
+{
+	tp_trigger_t trigger = p->trigger;
+	p->trigger = TP_TRIGGER_NONE;
+	if (trace_sched_thread_placement_enabled())
+		if (trigger && (trigger == TP_TRIGGER_NEW_THREAD || task_cpu(p) != cpu)) {
+			int level = lowest_sd_level_containing_both(task_cpu(p), cpu);
+			trace_sched_thread_placement(p, cpu, trigger, level);
+		}
+
+	set_task_rq(p, cpu);
+#ifdef CONFIG_SMP
+	/*
+	 * After ->cpu is set up to a new value, task_rq_lock(p, ...) can be
+	 * successfuly executed on another CPU. We must ensure that updates of
+	 * per-task data have been completed by this moment.
+	 */
+	smp_wmb();
+#ifdef CONFIG_THREAD_INFO_IN_TASK
+	p->cpu = cpu;
+#else
+	task_thread_info(p)->cpu = cpu;
+#endif
+	p->wake_cpu = cpu;
+#endif
+}
+
 void set_task_cpu(struct task_struct *p, unsigned int new_cpu)
 {
 #ifdef CONFIG_SCHED_DEBUG
@@ -1268,6 +1312,7 @@ static void __migrate_swap_task(struct task_struct *p, int cpu)
 
 		p->on_rq = TASK_ON_RQ_MIGRATING;
 		deactivate_task(src_rq, p, 0);
+		p->trigger = TP_TRIGGER_NUMA_BALANCING;
 		set_task_cpu(p, cpu);
 		activate_task(dst_rq, p, 0);
 		p->on_rq = TASK_ON_RQ_QUEUED;
@@ -2085,11 +2130,14 @@ try_to_wake_up(struct task_struct *p, unsigned int state, int wake_flags)
 	p->sched_contributes_to_load = !!task_contributes_to_load(p);
 	p->state = TASK_WAKING;
 
+	p->trigger = TP_TRIGGER_WAKE_UP_LB;
 	cpu = select_task_rq(p, p->wake_cpu, SD_BALANCE_WAKE, wake_flags);
 	if (task_cpu(p) != cpu) {
 		wake_flags |= WF_MIGRATED;
 		set_task_cpu(p, cpu);
 	}
+	p->trigger = TP_TRIGGER_NONE;
+
 #endif /* CONFIG_SMP */
 
 	ttwu_queue(p, cpu, wake_flags);
@@ -2574,6 +2622,7 @@ void wake_up_new_task(struct task_struct *p)
 	 * Use __set_task_cpu() to avoid calling sched_class::migrate_task_rq,
 	 * as we're not fully set-up yet.
 	 */
+	p->trigger = TP_TRIGGER_NEW_THREAD;
 	__set_task_cpu(p, select_task_rq(p, task_cpu(p), SD_BALANCE_FORK, 0));
 #endif
 	rq = __task_rq_lock(p, &rf);
@@ -2989,7 +3038,7 @@ void sched_exec(void)
 		goto unlock;
 
 	if (likely(cpu_active(dest_cpu))) {
-		struct migration_arg arg = { p, dest_cpu };
+		struct migration_arg arg = { p, dest_cpu, TP_TRIGGER_EXEC };
 
 		raw_spin_unlock_irqrestore(&p->pi_lock, flags);
 		stop_one_cpu(task_cpu(p), migration_cpu_stop, &arg);
@@ -5413,7 +5462,7 @@ static bool sched_smp_initialized __read_mostly;
 /* Migrate current task p to target_cpu */
 int migrate_task_to(struct task_struct *p, int target_cpu)
 {
-	struct migration_arg arg = { p, target_cpu };
+	struct migration_arg arg = { p, target_cpu, TP_TRIGGER_NUMA_BALANCING };
 	int curr_cpu = task_cpu(p);
 
 	if (curr_cpu == target_cpu)
@@ -5583,7 +5632,7 @@ static void migrate_tasks(struct rq *dead_rq)
 		/* Find suitable destination for @next, with force if needed. */
 		dest_cpu = select_fallback_rq(dead_rq->cpu, next);
 
-		rq = __migrate_task(rq, next, dest_cpu);
+		rq = __migrate_task(rq, next, dest_cpu, TP_TRIGGER_HOTPLUG);
 		if (rq != dead_rq) {
 			raw_spin_unlock(&rq->lock);
 			rq = dead_rq;
diff --git a/kernel/sched/deadline.c b/kernel/sched/deadline.c
index 37e2449..4a5cdc7 100644
--- a/kernel/sched/deadline.c
+++ b/kernel/sched/deadline.c
@@ -271,6 +271,7 @@ static struct rq *dl_task_offline_migration(struct rq *rq, struct task_struct *p
 		double_lock_balance(rq, later_rq);
 	}
 
+	p->trigger = TP_TRIGGER_DEADLINE;
 	set_task_cpu(p, later_rq->cpu);
 	double_unlock_balance(later_rq, rq);
 
@@ -1501,6 +1502,7 @@ static int push_dl_task(struct rq *rq)
 	}
 
 	deactivate_task(rq, next_task, 0);
+	next_task->trigger = TP_TRIGGER_DEADLINE;
 	set_task_cpu(next_task, later_rq->cpu);
 	activate_task(later_rq, next_task, 0);
 	ret = 1;
@@ -1589,6 +1591,7 @@ static void pull_dl_task(struct rq *this_rq)
 			resched = true;
 
 			deactivate_task(src_rq, p, 0);
+			p->trigger = TP_TRIGGER_DEADLINE;
 			set_task_cpu(p, this_cpu);
 			activate_task(this_rq, p, 0);
 			dmin = p->dl.deadline;
diff --git a/kernel/sched/fair.c b/kernel/sched/fair.c
index c242944..d4c0629 100644
--- a/kernel/sched/fair.c
+++ b/kernel/sched/fair.c
@@ -2883,6 +2883,7 @@ __update_load_avg(u64 now, int cpu, struct sched_avg *sa,
 		if (cfs_rq) {
 			cfs_rq->runnable_load_avg =
 				div_u64(cfs_rq->runnable_load_sum, LOAD_AVG_MAX);
+			trace_sched_load_update(LU_TRIGGER_PERIODIC, cpu, 0, cfs_rq->runnable_load_avg);
 		}
 		sa->util_avg = sa->util_sum / LOAD_AVG_MAX;
 	}
@@ -3163,6 +3164,7 @@ enqueue_entity_load_avg(struct cfs_rq *cfs_rq, struct sched_entity *se)
 	decayed = update_cfs_rq_load_avg(now, cfs_rq, !migrated);
 
 	cfs_rq->runnable_load_avg += sa->load_avg;
+	trace_sched_load_update(LU_TRIGGER_ENQUEUE, cpu_of(rq_of(cfs_rq)), 0, cfs_rq->runnable_load_avg);
 	cfs_rq->runnable_load_sum += sa->load_sum;
 
 	if (migrated)
@@ -3180,6 +3182,7 @@ dequeue_entity_load_avg(struct cfs_rq *cfs_rq, struct sched_entity *se)
 
 	cfs_rq->runnable_load_avg =
 		max_t(long, cfs_rq->runnable_load_avg - se->avg.load_avg, 0);
+	trace_sched_load_update(LU_TRIGGER_DEQUEUE, cpu_of(rq_of(cfs_rq)), 0, cfs_rq->runnable_load_avg);
 	cfs_rq->runnable_load_sum =
 		max_t(s64,  cfs_rq->runnable_load_sum - se->avg.load_sum, 0);
 }
@@ -4788,6 +4791,7 @@ static void cpu_load_update(struct rq *this_rq, unsigned long this_load,
 			new_load += scale - 1;
 
 		this_rq->cpu_load[i] = (old_load * (scale - 1) + new_load) >> i;
+		trace_sched_load_update(LU_TRIGGER_PERIODIC, cpu_of(this_rq), i, this_rq->cpu_load[i]);
 	}
 
 	sched_avg_update(this_rq);
@@ -5633,7 +5637,8 @@ select_task_rq_fair(struct task_struct *p, int prev_cpu, int sd_flag, int wake_f
 
 	if (sd_flag & SD_BALANCE_WAKE) {
 		record_wakee(p);
-		want_affine = !wake_wide(p) && !wake_cap(p, cpu, prev_cpu)
+		want_affine = sched_feat(WAKE_AFFINE)
+			      && !wake_wide(p) && !wake_cap(p, cpu, prev_cpu)
 			      && cpumask_test_cpu(cpu, tsk_cpus_allowed(p));
 	}
 
@@ -5660,6 +5665,7 @@ select_task_rq_fair(struct task_struct *p, int prev_cpu, int sd_flag, int wake_f
 
 	if (affine_sd) {
 		sd = NULL; /* Prefer wake_affine over balance flags */
+	  p->trigger = TP_TRIGGER_WAKE_AFFINE;
 		if (cpu != prev_cpu && wake_affine(affine_sd, p, prev_cpu, sync))
 			new_cpu = cpu;
 	}
@@ -6030,6 +6036,9 @@ pick_next_task_fair(struct rq *rq, struct task_struct *prev, struct pin_cookie c
 	return p;
 
 idle:
+	if (!sched_feat(IDLE_LOAD_BALANCING))
+		return NULL;
+
 	/*
 	 * This is OK, because current is on_cpu, which avoids it being picked
 	 * for load-balance and preemption/IRQs are still disabled avoiding
@@ -6259,6 +6268,7 @@ struct lb_env {
 	struct cpumask		*dst_grpmask;
 	int			new_dst_cpu;
 	enum cpu_idle_type	idle;
+	tp_trigger_t trigger;
 	long			imbalance;
 	/* The set of CPUs under consideration for load-balancing */
 	struct cpumask		*cpus;
@@ -6451,6 +6461,7 @@ static void detach_task(struct task_struct *p, struct lb_env *env)
 
 	p->on_rq = TASK_ON_RQ_MIGRATING;
 	deactivate_task(env->src_rq, p, 0);
+	p->trigger = env->trigger;
 	set_task_cpu(p, env->dst_cpu);
 }
 
@@ -7677,7 +7688,7 @@ static int should_we_balance(struct lb_env *env)
  * tasks if there is an imbalance.
  */
 static int load_balance(int this_cpu, struct rq *this_rq,
-			struct sched_domain *sd, enum cpu_idle_type idle,
+			struct sched_domain *sd, enum cpu_idle_type idle, tp_trigger_t trigger,
 			int *continue_balancing)
 {
 	int ld_moved, cur_ld_moved, active_balance = 0;
@@ -7693,6 +7704,7 @@ static int load_balance(int this_cpu, struct rq *this_rq,
 		.dst_rq		= this_rq,
 		.dst_grpmask    = sched_group_cpus(sd->groups),
 		.idle		= idle,
+		.trigger	= trigger,
 		.loop_break	= sched_nr_migrate_break,
 		.cpus		= cpus,
 		.fbq_type	= all,
@@ -8013,7 +8025,7 @@ static int idle_balance(struct rq *this_rq)
 			t0 = sched_clock_cpu(this_cpu);
 
 			pulled_task = load_balance(this_cpu, this_rq,
-						   sd, CPU_NEWLY_IDLE,
+						   sd, CPU_NEWLY_IDLE, TP_TRIGGER_IDLE_LB,
 						   &continue_balancing);
 
 			domain_cost = sched_clock_cpu(this_cpu) - t0;
@@ -8285,7 +8297,7 @@ void update_max_interval(void)
  *
  * Balancing parameters are set up in init_sched_domains.
  */
-static void rebalance_domains(struct rq *rq, enum cpu_idle_type idle)
+static void rebalance_domains(struct rq *rq, enum cpu_idle_type idle, tp_trigger_t trigger)
 {
 	int continue_balancing = 1;
 	int cpu = rq->cpu;
@@ -8336,7 +8348,7 @@ static void rebalance_domains(struct rq *rq, enum cpu_idle_type idle)
 		}
 
 		if (time_after_eq(jiffies, sd->last_balance + interval)) {
-			if (load_balance(cpu, rq, sd, idle, &continue_balancing)) {
+			if (load_balance(cpu, rq, sd, idle, trigger, &continue_balancing)) {
 				/*
 				 * The LBF_DST_PINNED logic could have changed
 				 * env->dst_cpu, so we can't know our idle
@@ -8429,7 +8441,7 @@ static void nohz_idle_balance(struct rq *this_rq, enum cpu_idle_type idle)
 			update_rq_clock(rq);
 			cpu_load_update_idle(rq);
 			raw_spin_unlock_irq(&rq->lock);
-			rebalance_domains(rq, CPU_IDLE);
+			rebalance_domains(rq, CPU_IDLE, TP_TRIGGER_NOHZ_PERIODIC_LB);
 		}
 
 		if (time_after(next_balance, rq->next_balance)) {
@@ -8539,6 +8551,9 @@ static __latent_entropy void run_rebalance_domains(struct softirq_action *h)
 	struct rq *this_rq = this_rq();
 	enum cpu_idle_type idle = this_rq->idle_balance ?
 						CPU_IDLE : CPU_NOT_IDLE;
+	tp_trigger_t trigger = test_bit(NOHZ_BALANCE_KICK, nohz_flags(this_rq->cpu))
+						? TP_TRIGGER_NOHZ_PERIODIC_LB
+						: TP_TRIGGER_STD_PERIODIC_LB;
 
 	/*
 	 * If this cpu has a pending nohz_balance_kick, then do the
@@ -8549,7 +8564,7 @@ static __latent_entropy void run_rebalance_domains(struct softirq_action *h)
 	 * and abort nohz_idle_balance altogether if we pull some load.
 	 */
 	nohz_idle_balance(this_rq, idle);
-	rebalance_domains(this_rq, idle);
+	rebalance_domains(this_rq, idle, trigger);
 }
 
 /*
diff --git a/kernel/sched/features.h b/kernel/sched/features.h
index 69631fa..57087c5 100644
--- a/kernel/sched/features.h
+++ b/kernel/sched/features.h
@@ -69,3 +69,6 @@ SCHED_FEAT(RT_RUNTIME_SHARE, true)
 SCHED_FEAT(LB_MIN, false)
 SCHED_FEAT(ATTACH_AGE_LOAD, true)
 
+SCHED_FEAT(WAKE_AFFINE, true)
+SCHED_FEAT(IDLE_LOAD_BALANCING, true)
+
diff --git a/kernel/sched/rt.c b/kernel/sched/rt.c
index 2516b8d..eeda94d 100644
--- a/kernel/sched/rt.c
+++ b/kernel/sched/rt.c
@@ -1842,6 +1842,7 @@ static int push_rt_task(struct rq *rq)
 	}
 
 	deactivate_task(rq, next_task, 0);
+	next_task->trigger = TP_TRIGGER_REALTIME;
 	set_task_cpu(next_task, lowest_rq->cpu);
 	activate_task(lowest_rq, next_task, 0);
 	ret = 1;
@@ -2096,6 +2097,7 @@ static void pull_rt_task(struct rq *this_rq)
 			resched = true;
 
 			deactivate_task(src_rq, p, 0);
+			p->trigger = TP_TRIGGER_REALTIME;
 			set_task_cpu(p, this_cpu);
 			activate_task(this_rq, p, 0);
 			/*
diff --git a/kernel/sched/sched.h b/kernel/sched/sched.h
index 055f935..64fef83 100644
--- a/kernel/sched/sched.h
+++ b/kernel/sched/sched.h
@@ -1011,24 +1011,7 @@ static inline struct task_group *task_group(struct task_struct *p)
 
 #endif /* CONFIG_CGROUP_SCHED */
 
-static inline void __set_task_cpu(struct task_struct *p, unsigned int cpu)
-{
-	set_task_rq(p, cpu);
-#ifdef CONFIG_SMP
-	/*
-	 * After ->cpu is set up to a new value, task_rq_lock(p, ...) can be
-	 * successfuly executed on another CPU. We must ensure that updates of
-	 * per-task data have been completed by this moment.
-	 */
-	smp_wmb();
-#ifdef CONFIG_THREAD_INFO_IN_TASK
-	p->cpu = cpu;
-#else
-	task_thread_info(p)->cpu = cpu;
-#endif
-	p->wake_cpu = cpu;
-#endif
-}
+inline void __set_task_cpu(struct task_struct *p, unsigned int cpu);
 
 /*
  * Tunables that become constants when CONFIG_SCHED_DEBUG is off:
